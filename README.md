# Chatbot

## Using Line Bot

To run using line bot, it is required to run the docker image with these environment variables:
```
LINE_CHANNEL_ACCESS_TOKEN=
LINE_CHANNEL_SECRET=
```

When using the `docker-compose.yml` file, said variables can be included in a `.env` file.