from typing import Callable

QuestionComparator = Callable[[str, str], bool]


class QAPair:

    def __init__(self, question: str, answer: str):
        self.question = question.lower()
        self.answer = answer
