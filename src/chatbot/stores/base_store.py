from abc import ABC, abstractmethod

from ..qa_pair import QAPair


class QueryableQAStore(ABC):

    @abstractmethod
    def query(self, question: str) -> QAPair:
        pass
