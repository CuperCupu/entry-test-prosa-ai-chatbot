from typing import List, Optional

from .base_store import QueryableQAStore
from ..qa_pair import QAPair, QuestionComparator


class InMemoryQAStore(QueryableQAStore):

    def __init__(self, comparator: QuestionComparator):
        self._store: List[QAPair] = []
        self.comparator = comparator

    def put(self, qa_pair: QAPair):
        if qa_pair not in self._store:
            self._store.append(qa_pair)

    def query(self, question: str) -> Optional[QAPair]:
        for qa_pair in self._store:
            if self.comparator(question, qa_pair.question):
                return qa_pair
        return None
