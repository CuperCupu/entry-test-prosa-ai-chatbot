import json

from .memory_store import InMemoryQAStore
from ..qa_pair import QAPair, QuestionComparator


class StoreBuilder:

    def __init__(self):
        self.source = None
        self.comparator = None

    def from_file(self, source: str):
        self.source = source
        return self

    def from_dictionary(self, source: dict):
        self.source = source
        return self

    def with_comparator(self, comparator: QuestionComparator):
        self.comparator = comparator
        return self

    def build(self):
        if self.source is None:
            raise TypeError("Source is not defined.")
        if self.comparator is None:
            raise TypeError("Comparator is not defined.")
        source = self.source
        if isinstance(source, str):
            if source.startswith("file://"):
                source = source[7:]
                if source.endswith('.json'):
                    with open(source) as f:
                        source = json.load(f)

        if isinstance(source, dict):
            return from_dict(source, self.comparator)
        elif isinstance(source, list):
            return from_list(source, self.comparator)


def store(source, comparator: QuestionComparator):
    return StoreBuilder().from_file(source).with_comparator(comparator).build()


def from_dict(mapping: dict, comparator: QuestionComparator):
    store = InMemoryQAStore(comparator)
    for key, value in mapping.items():
        store.put(QAPair(key, value))
    return store


def from_list(mapping: list, comparator: QuestionComparator):
    store = InMemoryQAStore(comparator)
    for key, value in mapping:
        store.put(QAPair(key, value))
    return store
