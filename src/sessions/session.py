from datetime import datetime


class Session:

    def __init__(self, session_id: str, expiration: datetime, channel: str):
        self.id = session_id
        self.expiration = expiration
        self.channel = channel
