from datetime import timedelta, datetime
from typing import Callable

import eventlet

from .session import Session
from .stores import BaseSessionStore, InMemorySessionStore


class SessionManager:

    def __init__(self, timeout: timedelta, store: BaseSessionStore = None):
        self.timeout = timeout
        self.store = InMemorySessionStore() if store is None else store
        self._session_expired_listeners = {}
        self._new_session = eventlet.Event()

    def on_session_expired(self, channel: str):
        def decorator(callback: Callable[[Session], None]):
            if channel in self._session_expired_listeners:
                listeners = self._session_expired_listeners[channel]
            else:
                listeners = []
                self._session_expired_listeners[channel] = listeners
            listeners.append(callback)
            return callback

        return decorator

    def expire_session(self, session_id: str):
        session = self.store.read(session_id)
        if session.channel in self._session_expired_listeners:
            for callback in self._session_expired_listeners[session.channel]:
                callback(session)
        self.delete(session_id)

    def clean_expired_sessions(self):
        current = datetime.now()
        to_expire = []
        for session in self.store:
            if current >= session.expiration:
                to_expire.append(session.id)

        for session_id in to_expire:
            self.expire_session(session_id)

    def run_cleanup(self):
        while True:
            self.clean_expired_sessions()
            now = datetime.now()
            nearest = None
            for session in self.store:
                if nearest is None:
                    nearest = session.expiration
                else:
                    if nearest > session.expiration:
                        nearest = session.expiration
            if nearest:
                delay = nearest - now
                delay_seconds = delay.total_seconds()
                self._new_session.wait(delay_seconds)
            else:
                self._new_session.wait()

    def create(self, session_id: str, channel: str) -> Session:
        session = Session(session_id, datetime.now() + self.timeout, channel)
        self.store.put(session)
        self._new_session.send()
        self._new_session.reset()
        return session

    def delete(self, session_id: str):
        self.store.delete(session_id)

    def refresh(self, session_id: str):
        session = self.store.read(session_id)
        session.expiration = datetime.now() + self.timeout
        self.store.put(session)
