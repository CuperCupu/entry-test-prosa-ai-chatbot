from abc import ABC, abstractmethod

from ..session import Session


class BaseSessionStore(ABC):

    @abstractmethod
    def read(self, session_id: str) -> Session:
        pass

    @abstractmethod
    def put(self, session: Session):
        pass

    @abstractmethod
    def delete(self, session_id: str):
        pass

    @abstractmethod
    def __iter__(self):
        pass
