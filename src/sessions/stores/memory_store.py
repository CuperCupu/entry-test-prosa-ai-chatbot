import threading

from .base_store import BaseSessionStore
from ..session import Session


class InMemorySessionStore(BaseSessionStore):

    def __init__(self):
        self._sessions = {}
        self._lock = threading.Lock()

    def put(self, session: Session):
        with self._lock:
            self._sessions[session.id] = session

    def read(self, session_id: str):
        with self._lock:
            if session_id not in self._sessions:
                raise KeyError(f"Unknown session id '{session_id}'")
            return self._sessions[session_id]

    def delete(self, session_id: str):
        with self._lock:
            if session_id in self._sessions:
                del self._sessions[session_id]

    def __iter__(self):
        return iter(self._sessions.values())
