import os

from app.web_socket import register_chatbot_websocket
from app.base import socketio, app


def main():
    host = os.environ.get("HOST", '0.0.0.0')
    port = os.environ.get("PORT", 80)

    @app.route('/')
    def index_page():
        return app.send_static_file('index.html')

    register_chatbot_websocket(socketio)
    if 'LINE_CHANNEL_ACCESS_TOKEN' in os.environ and 'LINE_CHANNEL_SECRET' in os.environ:
        from app.line_bot import bp, logger
        logger.info("Using line bot")
        app.register_blueprint(bp, url_prefix='/api/line')

    app.logger.info(f"Listening at {host if host else ''}:{port}")
    socketio.run(app, host=host, port=port)


if __name__ == '__main__':
    main()
