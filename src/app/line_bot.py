import os
from logging import getLogger, INFO

from flask import Blueprint, request, abort
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, TextMessage, TextSendMessage

from sessions import Session
from .chatbot_app import query
from .session import session

logger = getLogger('line_bot')
logger.setLevel(INFO)

bp = Blueprint('line_bot', __name__)

line_bot_api = LineBotApi(os.environ.get('LINE_CHANNEL_ACCESS_TOKEN'))
handler = WebhookHandler(os.environ.get('LINE_CHANNEL_SECRET'))


@bp.route("/callback", methods=['POST'])
def callback():
    signature = request.headers['X-Line-Signature']

    body = request.get_data(as_text=True)

    try:
        handler.handle(body, signature)
    except InvalidSignatureError as e:
        logger.error("Invalid signature. Please check your channel access token/channel secret.", exc_info=e)
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event: MessageEvent):
    logger.info(f'User {event.source.sender_id} queried: {event.message.text}')

    try:
        session.refresh(event.source.sender_id)
    except KeyError:
        session.create(event.source.sender_id, 'line_bot')

    result = query(event.message.text)
    if result:
        result = result.answer
    else:
        result = 'I am sorry, I do not know the answer to that'
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=result)
    )


@session.on_session_expired(channel='line_bot')
def closing_greet(s: Session):
    logger.info(f"Expiring session '{s.id}'")
    line_bot_api.push_message(s.id, TextSendMessage(text='Thank you for using our chatbot'))
