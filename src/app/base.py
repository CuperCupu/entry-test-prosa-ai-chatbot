import os
from logging import basicConfig, INFO

from flask import Flask
from flask_socketio import SocketIO


basicConfig(format='%(asctime)s: %(levelname)s - %(name)s: %(message)s')

app = Flask('chat_bot', static_url_path='/static', static_folder=os.path.join(os.getcwd(), 'static'))
app.config['SECRET_KEY'] = os.environ.get('FLASK_SECRET', '5k82QKSTR6xrB5LEQgUM')
app.logger.setLevel(INFO)
socketio = SocketIO(app)
