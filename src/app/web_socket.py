from logging import getLogger, INFO

from flask import request
from flask_socketio import emit

from sessions import Session
from .chatbot_app import query
from .session import session

logger = getLogger('web_socket')
logger.setLevel(INFO)


def register_chatbot_websocket(socketio):

    @socketio.on('query', namespace='/chat_bot')
    def chat_query(msg):
        logger.info(f'Client {request.sid} queried: {msg}')
        msg = msg.lower()
        result = query(msg)
        if result:
            emit('query response', result.answer)
        else:
            emit('query response', None)
        session.refresh(request.sid)

    @socketio.on('connect', namespace='/chat_bot')
    def connected():
        logger.info(f'Client {request.sid} connected')
        session.create(request.sid, 'web_socket')

    @socketio.on('disconnect', namespace='/chat_bot')
    def disconnected():
        logger.info(f'Client {request.sid} disconnected')
        session.delete(request.sid)

    @session.on_session_expired
    def session_expired(s: Session):
        logger.info(f"Expiring session '{s.id}'")
        socketio.server.emit('timeout', namespace='/chat_bot', room=s.id)
        socketio.server.disconnect(s.id)

    @session.on_session_expired(channel='web_socket')
    def session_expired(s: Session):
        logger.info(f"Expiring session '{s.id}'")
        socketio.server.emit('timeout', namespace='/chat_bot', room=s.id)
        socketio.server.disconnect(s.id)
