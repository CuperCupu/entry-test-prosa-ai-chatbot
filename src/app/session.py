import os
from datetime import timedelta

import eventlet

from sessions import SessionManager

inactive_timeout_seconds = float(os.environ.get('INACTIVE_TIMEOUT', 30))

session = SessionManager(timedelta(seconds=inactive_timeout_seconds))

eventlet.spawn(session.run_cleanup)
