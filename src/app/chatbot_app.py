import os
from logging import getLogger, INFO

import Levenshtein

from chatbot import store

logger = getLogger('chat_bot')
logger.setLevel(INFO)

qa_store_source = os.environ.get('QA_SOURCE', 'file://data.json')

logger.info(f"Using source '{qa_store_source}'")


def levenshtein_comparator(src: str, dst: str):
    src_words = src.lower().rstrip('?').split()
    dst_words = dst.rstrip('?').split()

    if len(src_words) != len(dst_words):
        return False

    for src_word, dst_word in zip(src_words, dst_words):
        dist = Levenshtein.distance(src_word, dst_word) > 1
        if dist:
            return False

    return True


qa_store = store(qa_store_source, levenshtein_comparator)

query = qa_store.query
