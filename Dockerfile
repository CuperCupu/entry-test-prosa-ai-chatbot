FROM python:3.8.5-alpine

RUN apk add build-base

WORKDIR chatbot

ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

FROM python:3.8.5-alpine

WORKDIR chatbot

COPY --from=0 /usr/local/lib/python3.8/site-packages /usr/local/lib/python3.8/site-packages

ADD data.json data.json
ADD static static
ADD src/ src/

ENV PYTHONPATH src

CMD python src/app