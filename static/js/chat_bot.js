$(document).ready(function(){
    let greeted = false;
    const socket = io.connect('http://' + document.domain + ':' + location.port + '/chat_bot');

    let closed = false;

    socket.on("connect", function() {
        chat_screen.addChatLeft("Connected to chatbot");
        if (!greeted) {
            chat_screen.addChatLeft("Hello");
            greeted = true;
        }
    });

    socket.on("disconnect", function() {
        if (!closed)
            chat_screen.addChatLeft("Disconnected from the chatbot, reconnecting...");
    });

    socket.on("timeout", function() {
        chat_screen.addChatLeft("You have been away for a while. If you need anything else just refresh the page. Thank you for using our chatbot. ");
        closed = true;
        socket.close();
        chat_screen.disableInput();
        chat_screen.clearListeners();
    });

    chat_screen.registerListener(function(text) {
        if (closed) {
            socket.connect();
        }
        if (socket.connected)
            socket.emit('query', text);
        else
            chat_screen.addChatLeft("I am currently unavailable, please try again later");
    });

    socket.on('query response', function(text) {
        if (text != null) {
            chat_screen.addChatLeft(text);
        } else {
            chat_screen.addChatLeft("I am sorry, I do not know the answer to that")
        }
    });
});