chat_screen = (function () {
    let message_latest_id = 0;

    const chatListeners = [];

    const registerListener = function (callback) {
        chatListeners.push(callback);
    };

    const clearListeners = function () {
        chatListeners.length = 0;
    };

    const invokeListeners = function (text) {
        chatListeners.forEach(listener => listener(text));
    };

    const generateMessageID = function () {
        return "message-" + message_latest_id++;
    };

    const scrollLatestChat = function () {
        $("#chat-viewport").scrollTop($("#chat-viewport")[0].scrollHeight);
    };

    const addChat = function (text, position) {
        var div = $("#chat-area");
        var chat = $(`<div id=${generateMessageID()} class="chat-line-${position}">
            <div class="talk-bubble talk-bubble-${position}">
                ${text}
            </div>
        </div>`).appendTo(div);
        scrollLatestChat();
        return chat;
    };

    const addChatRight = function (text) {
        const rv = addChat(text, 'right');
        invokeListeners(text);
        return rv;
    };

    const addChatLeft = function (text) {
        return addChat(text, 'left');
    };

    const input_field = $("#input-field");
    const send_button = $("#input-send");

    const disableInput = function () {
        input_field.prop("disabled", true);
        send_button.prop("disabled", true);
    };

    return {
        addChatLeft,
        addChatRight,
        scrollLatestChat,
        registerListener,
        clearListeners,
        disableInput
    }
})();

$(window).on("load", function () {

    chat_screen.scrollLatestChat();
    const input_field = $("#input-field");

    $("#input-send").on("click", (e) => {
        var text = input_field.val();
        if (text !== "") {
            chat_screen.addChatRight(text);
            input_field.val("");
        }
        input_field.focus();
    });


    input_field.on("keydown", (e) => {
        if ((e.keyCode && (e.keyCode === 13)) || (e.which && (e.which === 13))) {
            $("#input-send").click();
        }
    });

    input_field.focus();


    $(window).on("keydown", (e) => {
        if ((e.keyCode && (e.keyCode === 13)) || (e.which && (e.which === 13))) {
            const focused = $(':focus');
            const field = $("#input-field");
            if (focused !== field) {
                field.focus();
            }
        }
    });

});


